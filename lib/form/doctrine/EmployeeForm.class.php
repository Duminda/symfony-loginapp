<?php

/**
 * Employee form.
 *
 * @package    employeeMgt
 * @subpackage form
 * @author     Duminda Wanninayake
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class EmployeeForm extends BaseEmployeeForm
{
  public function configure()
  {
  }
}
