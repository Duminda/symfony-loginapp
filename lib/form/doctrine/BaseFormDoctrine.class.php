<?php

/**
 * Project form base class.
 *
 * @package    employeeMgt
 * @subpackage form
 * @author     Duminda Wanninayake
 * @version    SVN: $Id: sfDoctrineFormBaseTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
abstract class BaseFormDoctrine extends sfFormDoctrine
{
  public function setup()
  {
  }
}
