<div class="row">

<div id="tabs">
  <ul>
    <li><a href="#tabs-1">My Info</a></li>
    <li><a href="#tabs-2">Employee Data</a></li>
    <li><a href="#tabs-3">Leave</a></li>
  </ul>
  <div id="tabs-1">
    <table>
      <thead>
        <tr>
          <th>Fullname</th>
          <th>Address</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($users as $user): ?>
        <tr>
          <td><?php echo $user->getFullname() ?></td>
          <td><?php echo $user->getAddress() ?></td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
  <div id="tabs-2">
    <?php include_partial('employeedata'); ?>
  </div>
  <div id="tabs-3">
    <?php include_partial('leave'); ?>
  </div>
</div>
<!--
  <ul>
  <li>
  <a href="javascript:activateTab('page1')">Tab 1</a>
  </li>
  <li>
  <a href="javascript:activateTab('page2')">Tab 2</a>
  </li>
  </ul>
  <div id="tabCtrl">
  <div id="page1" style="display: block;">Page 1</div>
  <div id="page2" style="display: none;">Page 2</div>
  </div>
-->
</div>