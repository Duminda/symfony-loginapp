<?php

/**
 * user actions.
 *
 * @package    employeeMgt
 * @subpackage user
 * @author     Duminda Wanninayake
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class userActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {  
     $this->form = new UserForm();
  }

  public function executeShow(sfWebRequest $request)
  {
 
    $logindata = $request->getParameter('user');

    $q = Doctrine::getTable('User')
        ->createQuery('u')
        ->where('u.username = ?', $logindata['username'])
        ->andWhere('u.password = ?', $logindata['password']);

    $user = $q->fetchOne(array(), Doctrine_Core::HYDRATE_RECORD);

    //$this->forward404Unless($user);

    if(count($q->fetchArray()) > 0){
       $this->users = Doctrine::getTable('Employee')
        ->createQuery('e')
        ->where('e.employeeId = ?', $user->userId)
        ->execute();
    }else{
      $this->redirect('user/index');
    }
  }
}
