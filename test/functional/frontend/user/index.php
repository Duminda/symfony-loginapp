<?php
/** Functional tests for /user/index.
 *
 * @author Duminda Wanninayake
 *
 * @package employeeMgt
 * @subpackage test.user
 */
class frontend_user_indexTest extends Test_Case_Functional
{
  protected
    $_application = 'frontend',
    $_url;

  protected function _setUp(  )
  {
    $this->_url = '/user/index';
  }

  public function testSuccess(  )
  {
    /* The Form plugin has to be activated before it can be used. */
    //$this->_browser->usePlugin('form');

    $this->_browser->get('/user/index');
    //$this->assertStatusCode(200);
   
    $this->_browser->click('Login', array(
      'userid' => '2',
      'username'   => 'rajitha',
      'password'    => '456'
    ));

    $user = Doctrine::getTable('User')->find('user');

    var_dump($user);

    //$this->assertEquals(
      //'/user/show',
      //$this->_browser->getResponse()->getRedirectURL(),
      //'Expected browser to be redirected to the confirmation page.'
    //);

    //var_dump(Doctrine::getTable('User')->findOneByUsername('rajitha'));

     $this->assertNotNull(
      Doctrine::getTable('User')->findOneByUsername('rajitha'),
      'Expected Profile record to be created successfully.'
    );

    //var_dump(Doctrine::getTable('User'));

    //var_dump(Doctrine::getTable('User')->findOneByUsername('rajitha'));
    //$user = Doctrine::getTable('User')->findOneByUsername('rajitha');
    //var_dump($user);

   // $this->assertEquals(Doctrine::getTable('User')->findOneByUsername('rajitha'),'rajitha',
     // 'Expected Profile record to be created successfully.'
    //);

    //$this->assertFalse(
      //$this->_browser->getForm()->hasErrors(),
      //'Expected form to have no errors.'
    //);

    //$decoded = $this->_browser->getContent()->decodeJson();
    //$this->assertEquals(
      //'OK',
      //$decoded->status,
      //'Expected success status value.'
    //);

    //$this->_browser->post('/user/index', array(
        //'username'   => 'rajitha',
        //'password'    => '456'
      //s));

    //return NULL;
  }
}