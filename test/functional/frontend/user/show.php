<?php
/** Functional tests for /user/show.
 *
 * @author Duminda Wanninayake
 *
 * @package employeeMgt
 * @subpackage test.user
 */
class frontend_user_showTest extends Test_Case_Functional
{
  protected
    $_application = 'frontend',
    $_url;

  protected function _setUp(  )
  {
    $this->_url = '/user/show';
  }

  public function testSmokeCheck(  )
  {
    $this->_browser->get($this->_url);
    //$this->assertStatusCode(200);

    return NULL;
  }
}