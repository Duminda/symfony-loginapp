<?php
require_once dirname(__FILE__).'/../../bootstrap/functional.php';

class functional_frontend_userActionsTest extends sfPHPUnitBaseFunctionalTestCase
{
  protected function getApplication()
  {
    return 'frontend';
  }

  public function testDefault()
  {
    $browser = $this->getBrowser();

    $username = "duminda";
    $password = "123";
    $uid = 1;

    $browser->
      get('/user/index')->

      with('request')->begin()->
        isParameter('module', 'user')->
        isParameter('action', 'index')->
      end()->
      
      with('response')->begin()->
        isStatusCode(200)->
        checkElement('label', 'Username')->
        info('============ Index page loaded ============')->
      end()
    ;

    $q = Doctrine_Query::create()
      ->select('e.*')
      ->from('Employee e')
      ->where('e.employeeid = ?', $uid);
     
    $emp = $q->fetchOne();

    $browser->post('/user/show', array('user' => array(
      'username'   => $username,
      'password'    => $password
    )))->
    with('request')->begin()->
      isParameter('module', 'user')->
      isParameter('action', 'show')->
    end()->
  
    with('response')->begin()->
      isStatusCode(200)->
      
      info('============ Login success! Show page loaded ============')->
      //isStatusCode(304)->
      //info('============ Login failed ============')->
      
      checkElement('td', $emp->fullname)->
      info('============ Valid employee data loaded ============')->
      //debug()-> 
    end()
    ;
    
  }

}