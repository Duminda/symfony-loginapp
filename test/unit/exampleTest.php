<?php
 
include dirname(__FILE__).'/../bootstrap/unit.php';
 
new sfDatabaseManager($configuration);
Doctrine_Core::loadData(sfConfig::get('sf_data_dir').'/fixtures');
 
$t = new lime_test(1, new lime_output_color());
 
// begin testing your model class
$t->diag('->SuccessfulLogin()');
$user = Doctrine::getTable('User')->findOneByUsername('rajitha');
$pass = Doctrine::getTable('User')->findOneByPassword('456');
$uid = Doctrine::getTable('User')->findOneByUserId('2');

$array1 = array($user->getUsername(), $pass->getPassword());

$t->is_deeply($array1, array('rajitha', '456'), 'description: findByUsername() returns the User for the given username 
  and findByPassword() returns the User for the given username');

$t->diag('->employeeDataFound()');
$eid = Doctrine::getTable('Employee')->findOneByEmployeeId('2');
$user = Doctrine::getTable('Employee')->findOneByFullname('rajitha');
$pass = Doctrine::getTable('Employee')->findOneByAddress('456');

$t->is($eid->getEmployeeId(), $uid->getUserId(), 'id are equal');


//$t->is($user->getUsername(), 'duminda', '->findOneByUsername() returns the User for the given username');
//$t->is($user->getPassword(), '123', '->findOneByPassword() returns the User for the given username');



